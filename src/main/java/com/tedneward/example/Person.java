package com.tedneward.example;

import java.beans.*;
import java.util.*;

public class Person implements Comparable<Person> {
	private int age;
	private String name;
	private double salary;
	private String ssn;
	private boolean propertyChangeFired = false;
	private static int count;
	
	public static class AgeComparator implements Comparator<Person> {
		public int compare(Person one, Person two) {
			return one.getAge() - two.getAge();
		}
	}
	
	public Person() {
		this("", 0, 0.0d);
	}

	public Person(String n, int a, double s) {
		name = n;
		age = a;
		salary = s;
		ssn = "";
		count++;
	}

	public int count() {
		return count;
	}
	
	public int getAge() {
		return age;
	}

	public void setAge(int years) {
		if (years < 0) {
			throw new IllegalArgumentException("Age is invalid (less than 0)");
		}
		age = years;
	}

	public String getName() {
		return name;
	}

	public void setName(String id) {
		if (id == null) {
			throw new IllegalArgumentException("Name is invalid (null)");
		}
		name = id;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double money) {
		salary = money;
	}

	public String getSSN() {
		return ssn;
	}

	public void setSSN(String value) {
		String old = ssn;
		ssn = value;

		this.pcs.firePropertyChange("ssn", old, ssn);
		propertyChangeFired = true;
	}

	public boolean getPropertyChangeFired() {
		return propertyChangeFired;
	}

	public double calculateBonus() {
		return salary * 1.10;
	}

	public String becomeJudge() {
		return "The Honorable " + name;
	}

	public int timeWarp() {
		return age + 10;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Person) {
			Person p = (Person)other;
			return (this.name.equals(p.name) && this.age == p.age);
		}
		return false;
	}

	public String toString() {
		return "[Person name:" + getName() + " age:" + getAge() + " salary:" + getSalary() + "]";
	}

	@Override
	public int compareTo(Person other) {
		return (int)(other.salary - salary);
	}

	public static ArrayList<Person> getNewardFamily() {
		ArrayList<Person> family = new ArrayList<Person>();
		Person ted = new Person("Ted", 41, 250000.00);
     	Person charlotte = new Person("Charlotte", 43, 150000.00);
     	Person michael = new Person("Michael", 22, 10000.00);
     	Person matthew = new Person("Matthew", 15, 0.00);
		family.add(ted);
		family.add(charlotte);
		family.add(michael);
		family.add(matthew);
		return family;
	}

	// PropertyChangeListener support; you shouldn't need to change any of
	// these two methods or the field
	//
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}
}